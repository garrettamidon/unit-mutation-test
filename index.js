const testFunctions = require('./cartridges/int_easycode/scripts/helpers/test1');
let testParamVal = {};
let testReturnVal;

const setUp = (conditional) => {
    testFunctions.testParameterChange(testParamVal, conditional);
    testReturnVal = testFunctions.testReturnValue(conditional);
    testFunctions.testSpyFunction(conditional);
}


(() => {
    console.log('\n-------------------------');
    console.log('Running with true conditional');
    setUp(true);
    console.log('This should be 7: ' + testParamVal.value);
    console.log('This should be true: ' + testReturnVal);
    console.log('-------------------------\n');
    console.log('\n-------------------------');
    console.log('Running with false conditional');
    setUp(false);
    console.log('This should be 2: ' + testParamVal.value);
    console.log('This should be false: ' + testReturnVal);
    console.log('-------------------------\n');
})();