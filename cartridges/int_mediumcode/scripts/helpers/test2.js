const fibonacci = require('./fibonacci');

const generateGlobalRandom = () => {
    let randNum = Math.random() * 10;
    global.testVar = randNum;
}

const testFibonnaci = (value) => {
    generateGlobalRandom();
    return fibonacci(1, 0, 0);
}

module.exports = {
    testFibonnaci
}