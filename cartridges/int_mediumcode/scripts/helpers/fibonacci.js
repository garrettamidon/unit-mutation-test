const generateFibonacci = (value, prevValue, iteration) => {
    if (iteration < global.testVar) {
        return generateFibonacci(value + prevValue, value, ++iteration)
    } else {
        return value;
    }
}



module.exports = {
    generateFibonacci
}