const helper = require('./helper1');

const testParameterChange = (testParam, conditional) => {
    if (conditional) {
        testParam.value = 7;
    } else {
        testParam.value = 2
    }
}

const testReturnValue = (conditional) => {
    if (conditional) {
        return true;
    }
    return false;
}

const testSpyFunction = (conditional) => {
    if (conditional) {
        helper.trueFunction();
    } else {
        helper.falseFunction();
    }
}

module.exports = {
    testParameterChange,
    testReturnValue,
    testSpyFunction
}