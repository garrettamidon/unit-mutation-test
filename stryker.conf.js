var YAML = require('js-yaml');
var fs = require('fs');

/**
 * Negate files coming in from .istanbul.yml in the exclude portion so they will be negated in our grep
 * @param {Array} excludeFiles - array of file path strings
 * @returns {array} result - array of negated strings
 */
function negateFiles(excludeFiles) {
    var result = [];
    for (var files in excludeFiles) { // eslint-disable-line no-restricted-syntax
        var negatedFileName = '!' + excludeFiles[files];
        result.push(negatedFileName);
    }
    return result;
}

/**
 * Read the .istnabul.yml and get the excluda files in mocha
 * @returns {Array} strykerMutate - array of negated files form mocha configuration
 */
function getMochaExcludeFiles() {
    try {
        var yamlAsJson = YAML.safeLoad(fs.readFileSync('.istanbul.yml', 'utf8'));
        var excludedMochaFiles = yamlAsJson && yamlAsJson.instrumentation ? yamlAsJson.instrumentation.excludes : [];
        var strykerMutate = negateFiles(excludedMochaFiles);
        return strykerMutate;
    } catch (e) {
        console.log(e) //eslint-disable-line
        return [];
    }
}

/**
 * Read the files to mutate from the stryker.config.json
 * @returns {array} files to be mutated by stryker
 */
function getMutateFiles() {
    var sourceExists = fs.existsSync('./stryker.config.json');
    var strykerConfig = sourceExists ? JSON.parse(fs.readFileSync('./stryker.config.json')) : {};
    if (strykerConfig.imports) {
        return strykerConfig.imports;
    }
    return [];
}

module.exports = function(config) {
    var mochaExcludes = getMochaExcludeFiles();
    var filesToMutate = [...getMutateFiles(), ...mochaExcludes];
    config.set({
        mutator: "javascript",
        mutate: filesToMutate,
        packageManager: "npm",
        reporters: ["html", "clear-text", "progress"],
        files: ['./cartridges/**', './test/unit/**/*', './package.json'],
        transpilers: [],
        testFramework: "mocha",
        coverageAnalysis: "all",
        mochaOptions: {
            files: './test/unit/**/*.js'
        }
    });
  };

