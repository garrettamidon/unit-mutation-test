const assert = require('chai').assert;
const proxyquire = require('proxyquire').noCallThru().noPreserveCache();
const sinon = require('sinon');

describe('Helper1 Tests', () => {
    let consoleLogSpy;
    let helper1File = proxyquire('../../../../../cartridges/int_testcode/scripts/helpers/helper1', {});
    beforeEach(() => {
        consoleLogSpy = sinon.spy(console, 'log');
    });
    afterEach(() => {
        consoleLogSpy.resetHistory();
    });

    after(() => {
        consoleLogSpy.restore();
    })

    it('should console.log true', () => {
        helper1File.trueFunction();
        assert.isTrue(consoleLogSpy.calledWith(true), 'Should call console.log with true');
    });

    it('should console.log false', () => {
        helper1File.falseFunction();
        assert.isTrue(consoleLogSpy.calledWith(false), 'Should call console.log withfalse');
    });
});