const assert = require('chai').assert;
const proxyquire = require('proxyquire').noCallThru().noPreserveCache();
const sinon = require('sinon');

describe('Test1 Tests', () => {
    let fileToTest;
    let trueFuncSpy;
    let falseFuncSpy;

    before(() => {
        let helperMock = {
            trueFunction: () => { return true;},
            falseFunction: () => { return true;}
        }
        fileToTest = proxyquire('../../../../../cartridges/int_testcode/scripts/helpers/test1', {
            './helper1': helperMock
        });
        trueFuncSpy = sinon.spy(helperMock, 'trueFunction');
        falseFuncSpy = sinon.spy(helperMock, 'falseFunction');
    });
    afterEach(() => {
        trueFuncSpy.resetHistory();
        falseFuncSpy.resetHistory();
    });

    after(() => {
        trueFuncSpy.restore();
        falseFuncSpy.restore();
    });

    it('Should test testSpyFunction with true parameter', () => {
        fileToTest.testSpyFunction(true);
        assert(trueFuncSpy.calledOnce, 'testSpyFunction should call the helper trueFunction');
    });

    it('Should test testSpyFunction with false parameter', () => {
        fileToTest.testSpyFunction(false);
        assert(falseFuncSpy.calledOnce, 'testSpyFunction should call the helper trueFunction');
    });

    it('Should test testParameterChange with true parameter', () => {
        let testVar = {
            value: 10
        };
        fileToTest.testParameterChange(testVar, true);
        assert.equal(testVar.value, 7, 'testParameterFunction should change testVar to 7');
    });

    it('Should test testParameterChange with false parameter', () => {
        let testVar = { 
            value: 10
        };
        fileToTest.testParameterChange(testVar, false);
        assert.equal(testVar.value, 2, 'testParameterFunction should change testVar to 2');
    });

    it('Should test testReturnValue with true parameter', () => {
        const returnValue = fileToTest.testReturnValue(true);
        assert.isTrue(returnValue, 'testReturnValue should return true');
    });

    it('Should test testReturnValue with false parameter', () => {
        const returnValue = fileToTest.testReturnValue(false);
        assert.isFalse(returnValue, 'testReturnValue should return false');
    });
});