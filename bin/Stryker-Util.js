/* eslint no-console: 0 */

'use strict';
const fs = require('fs');
const prompt = require('prompt'); // Interactive Command Line Prompt
const optimist = require('optimist'); // Handle the parameters passed to this script
const log = require('pretty-log'); // Pretty Logging

const importDescription = 'Please enter: coreScripts (to mutate app_brand_core/**/scripts/**), ' +
    'storefrontScripts (to mutate app_storefront_base/**/scripts/**), ' +
    'bvScripts (to mutate int_bazaarvoice/**/scripts/**), ' +
    'otherScripts (to mutate all other scripts not included in other options), ' +
    'models (to mutate cartridges/**/models/**), ' +
    'anything else will result in all app_brand_core files being mutated';

/**
 * The Schema to be used for the command line prompt
 *
 * Properties:
 * - function
 * -- metaData (update meta data tag in reports html),
 * -- updateImport (update stryker.config.json file imports pattern),
 * -- createJSON (create reports JSON from the finished test run),
 * -- finalize (take all reports json and convert them into a consumable js for the report to read)
 * - import
 * -- coreScripts (update import with app_brand_core/scripts),
 * -- storefrontScripts (update import with app_storefront_base/scripts),
 * -- bvScripts (update import with int_bazaarvoice/scripts),
 * -- models (update import with all /models),
 * -- core (update import with app_brand_core models and scripts)
 * -- none (don't update import file)
 */
const schema = {
    properties: {
        function: {
            pattern: new RegExp('metaData|updateImport|createJSON|finalize'),
            message: 'function must be one of the following: metaData | updateImport | createJSON | finalize',
            description: 'Please enter: metaData (to update the meta tag in the html coverage for stryker), updateImport (to update the import files to be mutated by stryker), createJSON (to create JSON file from report), or finalize (to create final JSON from all reports)',
            required: true
        },
        import: {
            pattern: new RegExp('coreScripts|storefrontScripts|bvScripts|otherScripts|models|core|none'),
            message: 'import must be one of the following: coreScripts | storefrontScripts | bvScripts | core | models | none',
            description: importDescription,
            required: false,
            default: 'core'
        }
    }
};

/**
 * Update the stryker coverage report index.html to have the meta tag to show the emojis
 */
function updateMetaData() {
    let sourceExists = fs.existsSync('./reports/mutation/html/index.html');
    if (sourceExists) {
        let inputContents = fs.readFileSync('./reports/mutation/html/index.html').toString().split('\n');
        let writeMetaTag = false;
        let fileContent = '';
        for (let lines in inputContents) { // eslint-disable-line no-restricted-syntax
            if (inputContents[lines]) {
                let headTag = '<head>';
                let metaTag = '<meta charset="utf-8">';
                let currentLine = inputContents[lines];
                let headerTagIndex = currentLine.indexOf(headTag);
                if (headerTagIndex > -1) {
                    writeMetaTag = true;
                } else if (writeMetaTag && currentLine.indexOf(metaTag) === -1) {
                    fileContent += '    <meta charset="utf-8">\n';
                    writeMetaTag = false;
                }
                fileContent += currentLine + '\n';
            }
        }
        fs.writeFile('./reports/mutation/html/index.html', fileContent, (err) => {
            if (err) {
                log.error(err);
            }
        });
    }
}

/**
 * Update the stryker coverage report index.html to have the meta tag to show the emojis
 */
function createReportJSON() {
    let sourceExists = fs.existsSync('./reports/mutation/html/bind-mutation-test-report.js');
    if (sourceExists) {
        let inputContents = fs.readFileSync('./reports/mutation/html/bind-mutation-test-report.js').toString();
        let equalSignLocation = inputContents.indexOf('=');
        if (equalSignLocation > -1) {
            // Take substring of 1 past the equal sign to not include it and 1 less than length to not get ;
            let stringifiedJSON = inputContents.substring(++equalSignLocation, inputContents.length - 1);
            let formattedJSON = JSON.parse(stringifiedJSON);
            if (!fs.existsSync('./reports/mutation/JSON')) {
                fs.mkdirSync('./reports/mutation/JSON');
            }
            fs.writeFileSync('./reports/mutation/JSON/mutation-report' + new Date().getTime() + '.json', JSON.stringify(formattedJSON));
        }
    }
}

/**
 * Delete all files in folder, including folder.
 */
function deleteFolderRecursive() {
    let path = './reports/mutation/JSON';
    if (fs.existsSync(path)) {
        fs.readdirSync(path).forEach(function (file) {
            let curPath = path + '/' + file;
            if (fs.lstatSync(curPath).isDirectory()) {
                deleteFolderRecursive(curPath);
            } else { // delete file
                fs.unlinkSync(curPath);
            }
        });
        fs.rmdirSync(path);
    }
}

/**
 * Merge all JSON reports into a single JSON
 */
function mergeReportJSON() {
    let files = fs.existsSync('./reports/mutation/JSON/') ? fs.readdirSync('./reports/mutation/JSON/') : [];
    let resultingJSON = {};
    if (!files.length) {
        createReportJSON();
        files = fs.existsSync('./reports/mutation/JSON/') ? fs.readdirSync('./reports/mutation/JSON/') : [];
    }
    for (let i in files) { // eslint-disable-line no-restricted-syntax
        if (files[i]) {
            let fileToRead = fs.readFileSync('./reports/mutation/JSON/' + files[i]).toString();
            let fileAsJSON = JSON.parse(fileToRead);
            if (fileAsJSON.files) {
                if (resultingJSON.files) {
                    resultingJSON.files = {...resultingJSON.files, ...fileAsJSON.files}; // eslint-disable-line
                } else {
                    resultingJSON = fileAsJSON;
                }
            }
        }
    }
    fs.writeFileSync('./reports/mutation/JSON/mutation-report-final.json', JSON.stringify(resultingJSON));
}

/**
 * Merge all of the JSON reports into on and output to the js for the html report to consume
 */
function finalizeReport() {
    let sourceExists = fs.existsSync('./reports/mutation/html/bind-mutation-test-report.js');
    mergeReportJSON();
    let finalJSON = fs.existsSync('./reports/mutation/JSON/mutation-report-final.json');
    if (sourceExists && finalJSON) {
        let sourceContents = fs.readFileSync('./reports/mutation/html/bind-mutation-test-report.js').toString();
        let finalJSONContents = fs.readFileSync('./reports/mutation/JSON/mutation-report-final.json').toString();
        let equalSignLocation = sourceContents.indexOf('=');
        if (equalSignLocation > -1) {
            // Take substring of 1 past the equal sign to not include it and 1 less than length to not get ;
            let resultingContent = sourceContents.substring(0, ++equalSignLocation);
            resultingContent += finalJSONContents + ';';
            fs.writeFileSync('./reports/mutation/html/bind-mutation-test-report.js', resultingContent);
            deleteFolderRecursive();
        }
    }
}

/**
 * Update stryker-config.json with which files to mutate, based on which option is passed
 * @param {string} importString - string of which files to mutate
 */
function updateImport(importString) {
    let sourceExists = fs.existsSync('./stryker.config.json');
    let strykerConfig = sourceExists ? JSON.parse(fs.readFileSync('./stryker.config.json')) : {};
    switch (importString) {
        case 'otherScripts':
            strykerConfig.imports = [
                '**/cartridges/**/scripts/**/*.js',
                '!**/cartridges/app_brand_core/**/*.js',
                '!**/cartridges/app_storefront_base/**/*.js',
                '!**/cartridges/int_bazaarvoice/**/scripts/**/*.js'
            ];
            break;
        case 'models':
            strykerConfig.imports = [
                '**/cartridges/**/models/**/*.js'
            ];
            break;
        case 'coreScripts':
            strykerConfig.imports = [
                '**/cartridges/app_brand_core/**/scripts/**/*.js'
            ];
            break;
        case 'storefrontScripts':
            strykerConfig.imports = [
                '**/cartridges/app_storefront_base/**/scripts/**/*.js'
            ];
            break;
        case 'bvScripts':
            strykerConfig.imports = [
                '**/cartridges/int_bazaarvoice/**/scripts/**/*.js'
            ];
            break;
        default:
            // mutate both app_brand_core models/scripts
            strykerConfig.imports = [
                '**/cartridges/app_brand_core/**/models/**/*.js',
                '**/cartridges/app_brand_core/**/scripts/**/*.js'
            ];
            break;
    }
    fs.writeFileSync('./stryker.config.json', JSON.stringify(strykerConfig));
}

/**
 * Main function that runs and prompts user for input if missing from the command line.
 */
(function init() {
    prompt.override = optimist.argv;
    prompt.start();

    // Retrieve the results of the prompt and execute the site imports
    prompt.get(schema, function (err, results) {
        switch (results.function) {
            case 'metaData':
                log.success('Updating Meta Data');
                updateMetaData();
                break;
            case 'updateImport':
                log.success('Updating Mutant Import files with: ' + results.import);
                updateImport(results.import);
                break;
            case 'createJSON':
                log.success('Creating results as JSON');
                createReportJSON();
                break;
            case 'finalize':
                log.success('Finalizing Report');
                finalizeReport();
                break;
            default:
        }
    });
}());
